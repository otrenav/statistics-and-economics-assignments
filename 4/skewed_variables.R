##
## Home Depot vs Lowe's Stores main
##
## Objective of this file: find skewed variables
##
## We'll use histograms to find out which variables are skewed
## and consequently know which need to be transformed using
## a logarithmic transformation to achieve better results.
## We'll do so by looking at the histograms.
##
## We will not transform any of the percentage variables.
##
## Omar Trejo
## March, 2017
##

hist(data[, "Lcount"])       ## Transform: yes
hist(data[, "HDcount"])      ## Transform: yes
hist(data[, "pop_2000"])     ## Transform: yes
hist(data[, "pop_2010"])     ## Transform: yes
hist(data[, "income_2000"])  ## Transform: no
hist(data[, "income_2010"])  ## Transform: no
