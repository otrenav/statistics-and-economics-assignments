##
## Home Depot vs Lowe's Stores functions
##
## Omar Trejo
## Marc, 2017
##

DIFF_VARIABLES <- c(
    "areaname",
    "county",
    "state",
    "r1",
    "r2",
    "income_difference",
    "pct_U18_difference",
    "pctcollege_difference",
    "ownhome_difference",
    "density_difference",
    "pctwhite_difference",
    "pctblack_difference"
)

YEAR_VARIABLES <- c(
    "areaname",
    "county",
    "state",
    "r1",
    "r2",
    "income_2000",
    "income_2010",
    "pct_U18_2000",
    "pct_U18_2010",
    "pctcollege_2000",
    "pctcollege_2010",
    "ownhome_2000",
    "ownhome_2010",
    "density_2000",
    "density_2010",
    "pctwhite_2000",
    "pctwhite_2010",
    "pctblack_2000",
    "pctblack_2010"
)

prepare <- function(data) {
    ##
    ## `county`, `r1`, and `r2` are categorical variables and need
    ## to be changed so that the models understand how to treat them
    ## correctly
    ##
    data$county                <- as.factor(data$county)
    data$r1                    <- as.factor(data$r1)
    data$r2                    <- as.factor(data$r2)

    ## Logarithmic transformations
    data$HDcount_ln            <- log(data$HDcount)
    data$Lcount_ln             <- log(data$Lcount)
    data$pop_2000_ln           <- log(data$pop_2000)
    data$pop_2010_ln           <- log(data$pop_2010)

    ## Difference variables
    data$HD_vs_L               <- data$HDcount - data$Lcount
    data$pop_difference        <- data$pop_2010 - data$pop_2000
    data$income_difference     <- data$income_2010 - data$income_2000
    data$pct_U18_difference    <- data$pct_U18_2010 - data$pct_U18_2000
    data$pctcollege_difference <- data$pctcollege_2010 - data$pctcollege_2000
    data$ownhome_difference    <- data$ownhome_2010 - data$ownhome_2000
    data$density_difference    <- data$density_2010 - data$density_2000
    data$pctwhite_difference   <- data$pctwhite_2010 - data$pctwhite_2000
    data$pctblack_difference   <- data$pctblack_2010 - data$pctblack_2000
    data$pop_ln_difference     <- data$pop_2010_ln - data$pop_2000_ln

    return(data)
}

get_HD_non_logarithmic_data <- function(data) {
    non_logarithmic_variables <- c(
        "HDcount",
        "pop_2000",
        "pop_2010",
        YEAR_VARIABLES
    )
    return(remove_useless_variables(data[, non_logarithmic_variables]))
}

get_HD_logarithmic_data <- function(data) {
    ##
    ## We need to remove those observations that had a value of
    ## zero because ln(0) == -Inf and those values are useless
    ## for the regression analysis. Which means that for logarithmic
    ## data, we'll focus only on those places that have at least
    ## one store. The same applies for all logarithmic data.
    ##
    logarithmic_variables <- c(
        "HDcount_ln",
        "pop_2000_ln",
        "pop_2010_ln",
        YEAR_VARIABLES
    )
    data <- data[, logarithmic_variables]
    data <- data[data$HDcount_ln >= 0, ]
    data <- data[data$pop_2000_ln >= 0, ]
    data <- data[data$pop_2010_ln >= 0, ]
    return(remove_useless_variables(data))
}

get_L_non_logarithmic_data <- function(data) {
    non_logarithmic_variables <- c(
        "Lcount",
        "pop_2000",
        "pop_2010",
        YEAR_VARIABLES
    )
    return(remove_useless_variables(data[, non_logarithmic_variables]))
}

get_L_logarithmic_data <- function(data) {
    logarithmic_variables <- c(
        "Lcount_ln",
        "pop_2000_ln",
        "pop_2010_ln",
        YEAR_VARIABLES
    )
    data <- data[, logarithmic_variables]
    data <- data[data$Lcount_ln >= 0, ]
    data <- data[data$pop_2000_ln >= 0, ]
    data <- data[data$pop_2010_ln >= 0, ]
    return(remove_useless_variables(data))
}

get_HD_non_logarithmic_diff_data <- function(data) {
    non_logarithmic_variables <- c(
        "HDcount",
        "pop_ln_difference",
        DIFF_VARIABLES
    )
    return(remove_useless_variables(data[, non_logarithmic_variables]))
}

get_HD_logarithmic_diff_data <- function(data, with_categoricals = FALSE) {
    logarithmic_variables <- c(
        "HDcount_ln",
        "pop_ln_difference",
        DIFF_VARIABLES
    )
    if (with_categoricals) {
        logarithmic_variables <- c(logarithmic_variables, "HDcount")
    }
    data <- data[, logarithmic_variables]
    data <- data[data$HDcount_ln >= 0, ]
    data <- data[data$pop_ln_difference >= 0, ]
    if (with_categoricals) {
        return(data)
    } else {
        return(remove_useless_variables(data))
    }
}

get_L_non_logarithmic_diff_data <- function(data) {
    non_logarithmic_variables <- c(
        "Lcount",
        "pop_ln_difference",
        DIFF_VARIABLES
    )
    return(remove_useless_variables(data[, non_logarithmic_variables]))
}

get_L_logarithmic_diff_data <- function(data, with_categoricals = FALSE) {
    logarithmic_variables <- c(
        "Lcount_ln",
        "pop_ln_difference",
        DIFF_VARIABLES
    )
    if (with_categoricals) {
        logarithmic_variables <- c(logarithmic_variables, "Lcount")
    }
    data <- data[, logarithmic_variables]
    data <- data[data$Lcount_ln >= 0, ]
    data <- data[data$pop_ln_difference >= 0, ]
    if (with_categoricals) {
        return(data)
    } else {
        return(remove_useless_variables(data))
    }
}

remove_useless_variables <- function(data) {
    variables <- colnames(data)
    variables <- variables[!grepl("county", variables)]
    variables <- variables[!grepl("state", variables)]
    variables <- variables[!grepl("areaname", variables)]
    return(data[, variables])
}
