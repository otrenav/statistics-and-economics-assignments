##
## Home Depot vs Lowe's Stores main
##
## Objective: assess how these two chains make decisiones on where
##            to locate stores, whether they are different, and wehther
##            they should consider opening new stores in the future.
##
## Omar Trejo
## March, 2017
##

setwd(".")
require(ggplot2)
source("./functions.R")

data <- read.csv("data.csv")
data <- prepare(data)

##
## Prepare data
##

## Year data
HD_non_logarithmic_data <- get_HD_non_logarithmic_data(data)
HD_logarithmic_data     <- get_HD_logarithmic_data(data)
L_non_logarithmic_data  <- get_L_non_logarithmic_data(data)
L_logarithmic_data      <- get_L_logarithmic_data(data)

## Diff data
HD_non_logarithmic_diff_data <- get_HD_non_logarithmic_diff_data(data)
HD_logarithmic_diff_data     <- get_HD_logarithmic_diff_data(data)
L_non_logarithmic_diff_data  <- get_L_non_logarithmic_diff_data(data)
L_logarithmic_diff_data      <- get_L_logarithmic_diff_data(data)

##
## Analysis
##
print("HD non-logarithmic model")
summary(lm(HDcount ~ . , HD_non_logarithmic_data))
print("HD logarithmic model")
summary(lm(Lcount ~ . ,  L_non_logarithmic_data))
print("L non-logarithmic model")
summary(lm(HDcount_ln ~ . , HD_logarithmic_data))
print("L logarithmic model")
summary(lm(Lcount_ln ~ . ,  L_logarithmic_data))

print("HD diff non-logarithmic model")
summary(lm(HDcount ~ . , HD_non_logarithmic_diff_data))
print("HD diff logarithmic model")
summary(lm(Lcount ~ . ,  L_non_logarithmic_diff_data))
print("L diff non-logarithmic model")
summary(lm(HDcount_ln ~ . , HD_logarithmic_diff_data))
print("L diff logarithmic model")
summary(lm(Lcount_ln ~ . ,  L_logarithmic_diff_data))

## NOTE: We see that `r2` shows NA's in the results possibly
## because it's a close to being a linear combination of the
## regions in `r1` (which is less aggregated). The county and
## state data is left out of the analysis because it did not
## yield particularly useful results (probably because the
## effect is captured in the first region variable).

##
## 1. How do these chains make their decisions about where to have
##    store locations? What are the major criteria that drive the
##    decision, and cand you provie a very brief reationalization
##    for each? How are they similar in their decision making?
##
## To explain their strategies, we use a regression analysis
## with the number of stores per county as the regressed variable
## against region and demographic data.
##
## By looking at the logarithmic models, we find that both companies
## located their stores where there was a high level of population
## as well as slightly low density in 2000 (the coefficient sign is
## negative). They both also seem to pursue regions where density has
## increased towards 2010, which can be seen density coefficient for the
## year-to-year models as well as the difference models, and it's very
## statistically significant. Finally they both also seem to be slightly
## focused towards places that don't have very high incomes in 2010.
## These last effects although not very strong, are still moderately
## statistically significant. It appears that both stores are looking
## for regions with open spaces (not so high density) and that do not
## have very high income levels. This is inline with the "DIY" customer
## profiles. They both want to go after that specifich niche. This makes
## sense since this kind of profile is very well aligned with these stores
## since their early years.
##
## 2. Are there ways in which the two chains are different in the
##    types of locations they target? What are those differences,
##    and why do you think that they may be apparent in the data?
##    Characterize the targeting strategies for each of the two
##    chains.
##
## Using non-logarithmic models, we can see that Home Depot's
## strategy shifted away from going after a market composed primarily
## by white population. This can be seen both in the year-to-year models
## as well as the difference models. Also it seems that it shifed away
## from counties with a high percentage of people under 18 years of age
## and a lesser percentage of population with college degrees. However
## these effects can only be seen clearly in the year-to-year models.
## They are not clear in the difference models.
##
## In the case of Lowe's, we see that they shifted away from counties
## composed primarily of black populatins but the effect is not
## statistically significant in the year-to-year models, although it
## is in the difference models. However, they do seem to be going
## towards counties that have a high percentage of people under 18
## years of age and high percentage of populatin with college degrees.
## They also seem to be slightly more narrow in their region focus.
##
## Some important considerations are that we don't know the method in
## which the data was obtained and what type of sampling techniques
## were used, so there may be non-randomized effects that we're not
## taking into account. Also, outliers may be pulling the results
## in a certain direction and to be certain we would need to perform
## a residuals analysis. Finally, we need to take into account
## demographic dynamics; we're just looking at two years in this
## data and we don't have a clear idea of the dynamics within, which
## may be another factor of weakness in this analysis.
##
## 3. What counties appear underserved in the data, by one or both
##    store chains? Where would you expect Home Depot to open its
##    next 2-3 locations? Lowes?
##
## In the following graphs we show the difference from 2000 to 2010
## in population in the x-axis, in density in the y-axis, in income
## by color (red is a big increment and blue is a big decrement),
## and in number of stores in the county for each company.
##
## We see a similar pattern in both graphs: as we move along the
## x-axis we see that we have a high number of stores, but as
## we move along the y-axis we find that there's a small number
## of stores for both companies. This means that there are
## opportunities to serve better those counties that have not
## had much population increase but have showed changes in it's
## density (meaning more compressed regions where it's going to
## be easier to serve various people).
##
## NOTE: There should be some interpreation regarding states and
## counties but since I don't know general US demographics I'm not
## including it.
##

ggplot(data = data,
       aes(x = pop_difference,
           y = density_difference,
           color = income_difference,
           size = HDcount)) +
    geom_point() +
    scale_color_gradient(
        low = "blue",
        high = "red"
    )

ggplot(data = data,
       aes(x = pop_difference,
           y = density_difference,
           color = income_difference,
           size = Lcount)) +
    geom_point() +
    scale_color_gradient(
        low = "blue",
        high = "red"
    )

##
## In the following graph we can see a comparison among Home Depot's
## and Lowe's location difference for each county. This means that
## the HD_vs_L metric contains the number of HD locations minus the
## number of L locations for a county. This means that is, for example,
## 5, then Home Depot has 5 more stores in that county than Lowes. If
## the number is -3, it means that Lowe's has 3 more stores in that
## county than Home Depot. If the number is 0 it means that they have
## the same number of stores in that county.
##
## As can be seen, in most counties Home Depot has slightly more stores
## but there are some outliers where it has more than twenty more
## stores than Lowe's. Most of the points are in the blue/purple zone,
## meaning that mostly they are balanced. However we could say that
## Lowe's could maybe benefit by putting stores in those counties
## that are red.
##
ggplot(data = data,
       aes(x = pop_difference,
           y = density_difference,
           color = HD_vs_L,
           size = pctwhite_difference)) +
    geom_point() +
    scale_color_gradient(
        low = "blue",
        high = "red"
    )

## To answer the question specifically, we can check which
## areas were predicted to have a higher number of stores, for
## each company, than what they actually have in the data. Those
## would be logical places to expand to. I'll use the logarithmic
## difference models which are more conservative.
##
## Note that we're using the exp() function to get back to a unit
## being equal to a single store (which was broken with the
## logarithmic transform) after using the logarithmic models.
##

##
## Home Depot
##
## In this case it appears that the New York (NY), Alexandria (VA),
## Suffolk (MA), Bronx (NY), and Hudson (NJ) are the next top 5
## choices for Home Depot, with an extra number of stores predicted
## to be 3.9, 1.7, 1.7, 1.6, and 1.6, respectively.
##
HD_data <- get_HD_logarithmic_diff_data(
    data,
    with_categoricals = TRUE
)
difference <- predict(lm(HDcount_ln ~ . , HD_logarithmic_diff_data))
diference <- exp(difference) - exp(HD_logarithmic_diff_data$HDcount_ln)
HD_data$HD_prediction_difference <- difference
head(HD_data[
    with(HD_data, order(-HD_prediction_difference)),
    c("areaname", "state", "county", "HDcount", "HD_prediction_difference")
])

##
## Lowe's
##
## In this case it appears that Hudson (NJ), San Francisco (CA),
## Kings (NY), Gwinnett (GA), and Fredericksburg (VA) are the next
## top 5 choices for Lowe's, with an extra number of stores predicted
## to be 2.6, 2.4, 2.2, 2.0, and 1.9, respectively.
##
L_data <- get_L_logarithmic_diff_data(
    data,
    with_categoricals = TRUE
)
difference <- predict(lm(Lcount_ln ~ . , L_logarithmic_diff_data))
diference <- exp(difference) - exp(L_logarithmic_diff_data$Lcount_ln)
L_data$L_prediction_difference <- difference
head(L_data[
    with(L_data, order(-L_prediction_difference)),
    c("areaname", "state", "county", "Lcount", "L_prediction_difference")
])
