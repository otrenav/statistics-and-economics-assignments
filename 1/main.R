##
## Assignment 1: Major League Baseball Team and Player Performance Analysis
##
## Omar Trejo,
## February 2017
##

setwd(".")

require(ggplot2)

##
## 1st section
##

runs <- read.csv("./baseball_ba_ops_runs.csv")

##
## a)
##

## Plot of wins vs runs
ggplot(runs, aes(x = R, y = W)) +
    geom_point() +
    labs(title = "A1-1-a) Wins vs runs")

##
## Each run produces 0.07804 wins
##
## Basic interpration: when a player produces a run for
## his team he brings his team closer to a win, which
## means that the regression coefficient is positive.
## It's not a large coefficient, but it it's still
## positive.
##
lm(W ~ R, data = runs)

##
## Each run allowed produces -0.08418 wins (i.e. losses)
##
## Basic interpretation: when a run is allowed, it benefits
## the other team, meaning that you're further away from a
## win. This is shown in in the negative sign in the
## coefficient. Notice that it's slightly stronger than
## producing a run, meaning you should be more careful
## about not allowing runs than producing runs, even if
## it seems counterintuitive. However, the difference
## isn't much.
##
lm(W ~ RA, data = runs)

##
## i) Regression of wins vs everything else
## Note: I needed to remove all variables after the
## 19th variable because they don't have any data.
##
## Basic interpretation:
##
## - Losses are perfectly and inversly associated with wins,
## meaning everytime you win, you don't loose, and viceversa,
## that's why its coefficient is -1.
##
## - In this case, since a lot of the information is being
## explained by other variables, the effect of runs on wins
## gets dilluted to only .0001, which is negligent. The same
## dillution effect can be seen in the runs allowed coefficeint
## which is now 0.0009, but an important effect is that it's now
## positive, which would indicate that every run allowed increases
## your odds of wining, which is obviously false. This effect is
## seen from the fact that it's not statistically significant
## (to see this use the summary command and look at the last column).
##
## - Given what was said about statistical significance, we'll only
## explain the other statistically significant variable which is AB
## (I'm assuming it's "At Bat"). It's very significant (Pr(>|t|) <= 0.005)
## and it's coefficient is 0.001, meaning that the more a team is
## batting, the better the odds of winning, although the coefficient is
## very low. All other variables are not statistically significant.
##
## - Note that the HR coefficient is `NA`, which means that this variable
## is very close to being a linear combination of the other variables.
## This indicates a multicolinearity problem with that variable.
##
lm(W ~ ., data = runs[, seq(3, 19)])
## summary(lm(W ~ ., data = runs[, seq(3, 19)]))

##
## ii) The result for each team is printed in the console
## NOTE: not all teams contain enough data to produce
## enough variance so that the using all variables makes sense.
## We're dealing with coliniearity in the data. `NA` is
## printed when we are faced with this problem.
##
## Basic interpretation: I won't interpret every team's results,
## but you can find some counterintuitive behavior in some of
## the results. For example:
##
## - For team St. Louis Cardinals we see that runs have a
## coefficient of -0.06, which seems larger than before, but
## can't know if it's statistically significant because the
## data is not allowing for this metrics to be computed.
##
## - In some cases you see a large coefficient for runs and
## a low coefficient for at bat, and viceversa. For example,
## Washington Nationals has a runs coefficient of 0.07 and
## a at bat coefficient of 0.003, while San Francisco Giants
## has 0.01 and 0.01, respectively. This, intuitively, shows
## the relation between being effective at bat or securing runs,
## but I don't know enough about baseball to know if this is
## actually something that makes sense.
##
## - There's an strange case for Los Angeles Angels of Anaheim,
## where we see that those same coefficients are both 0, and
## not only these but other coefficients are also zero, meaning
## that the data for this team does not appear to be correlated
## with their wins.
##
unique_teams <- unique(runs$name)
for (team in unique_teams) {
    print(paste("Current team: ", team, sep = ""))
    team_data <- runs[runs$name == team, ]
    print(lm(W ~ ., data = team_data[, seq(3, 19)]))
    ## print(summary(lm(W ~ ., data = team_data[, seq(3, 19)])))
}

##
## b) I don't have the book, but I'll use this resource:
## http://www.baseball-reference.com/bullpen/Pythagorean_Theorem_of_Baseball
##
## According to Baseball's Pythagorean Theorem
win_percentage <- runs$R^2 / (runs$R^2 + runs$RA^2)
runs$pythagorean_error <- win_percentage - (runs$W / (runs$W + runs$L))

## Maximum error produced by Baseball's
## Pythagorean Theorem is off by 0.07 W%,
## meaning that it's very close to the
## real results.
max(runs$pythagorean_error)

##
## c)
##

##
## i) Regress runs scored vs all variables
##
## Basic intepretation:
##
## - We can see that H, X1B, X2B, BB, and HBP are statistically
## significant using the `summary()` function. We'll try to
## interpret those.
##
## - Every hit contributes 1.16 runs, which makes sense since
## when a team produces a hit it's closer to scoring a run.
##
## - Both X1B and X2B are not good for a team. They mean that
## the other team produced one or two outs against your team
## in a single play, respectively. They push away from scoring.
## However, it seems that producing a single out has a stronger
## effect towards avoiding a run than producing two outs in
## the same play (I don't know enough baseball to make sense
## of this).
##
## - Base on balls bring you closer to scoring a run according
## to the 0.23 coefficient, which is lower than the hit coefficient,
## and the hit by pitch coefficient which is 0.3. However all of
## them bring you closer to scoring a run.
##
lm(R ~ ., data = runs[, seq(3, 19)])
summary(lm(R ~ ., data = runs[, seq(3, 19)]))

##
## ii) I didn't find Winton's Linear Weights and I don't have
## the book so I can't check this. I'm sorry.
##

##
## iii) Find which teams produced more/less runs than expected?
##
## NOTE: since we have data for teams and years, we can learn
## which teams were above their predicted scored runs for a given
## year, which is what I do here (it's a bit more detailed than
## what you were asked for).

runs$predicted_runs <- predict(lm(R ~ ., data = runs[, seq(3, 19)]))
runs$above_prediction <- runs$R > runs$predicted_runs

## Teams/year above prediction
print(runs[runs$above_prediction, c("name", "yearID")])

## Teams/year below or equal to prediction
print(runs[!runs$above_prediction, c("name", "yearID")])

##
## 2nd section
##

players <- read.csv("./player_batting_value_add.csv")

##
## a) I don't understand what this is requesting
##

##
## b) I don't understand what this is requesting
##
