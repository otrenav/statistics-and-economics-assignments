##
## Assignment 2: Golf Performance Measrument
##
## Omar Trejo,
## February 2017
##

setwd(".")

data <- read.csv("./riccio_project_williams.csv")

##
## Cleaning data
##

##
## Earnings:
##
## We need to transform earnings from string to
## numbers to be able to use it in regressions:
## This first example I'm doing it step by step
## so that you can see how it works, the following
## examples I'll do more succintly.

## 1st: it's a categorical (factor) variable
## that needs to be a string variable
data$earnings <- as.character(data$earnings)

## 2nd: remove "," characters
data$earnings <- gsub(",", "", data[, "earnings"])

## 3rd: remove spaces (" ")
data$earnings <- gsub(" ", "", data[, "earnings"])

## 4th: remove "$"
data$earnings <- gsub("[[:punct:]]", "", data[, "earnings"])

# 5th Convert to numeric variable
data$earnings <- as.numeric(data$earnings)

## Cleaning of other variables
data$total_strokes <- as.numeric(gsub(",", "", data$total_strokes))
data$total_driving_distance <- as.numeric(gsub(",", "", data$total_driving_distance))
data$average_driving_distance <- as.numeric(gsub(",", "", data$average_driving_distance))
data$total_putts <- as.numeric(gsub(",", "", data$total_putts))
data$fairway <- as.numeric(gsub("%", "", data$fairway)) / 100
data$gir <- as.numeric(gsub("%", "", data$gir)) / 100

##
## a)
##

lm(earnings ~ average_strokes, data)
summary(lm(earnings ~ average_strokes, data))

## NOTE: I'm assuming that the variable "average_strokes" in
## the data can be interpreted as "a player's average score
## per round".

## Yes, average strokes is a very statistically significant predictor
## for earnings, as we can see by having p-value of almost 0.

##
## a)
##

##
## i)
##

## Since we don't have "money winnings per event entered"
## all we do is divide "earnings" by "events" to get it.
data$earnings_per_event <- data$earnings / data$events

lm(earnings_per_event ~ average_strokes, data)
summary(lm(earnings_per_event ~ average_strokes, data))

## Doing it this way we see that "average strokes" is still a
## very statistically significant regression for "earnings".

##
## ii)
##

## NOTE: If we don't take into account the distribution of
## earnings we may think in creating three groups by dividing
## max - min / 3, but this ends up having all groups in the
## last bucket because the data is heavily skewed. I'm just
## leaving this here as a reference for you to spot this
## problems later on.

## first_limit  <- (max(data$earnings) - min(data$earnings)) * 1/3
## second_limit <- (max(data$earnings) - min(data$earnings)) * 2/3

## What we want to do is use quantiles, which take into account
## the distribution. I'll create three groups using quantiles,
## which more or less leaves the groups observations balanced.

first_limit  <- quantile(data$earnings, 0.3)
second_limit <- quantile(data$earnings, 0.6)

data[data$earnings <= first_limit, "earnings_group"] <- 1
data[data$earnings >  first_limit & data$earnings <= second_limit, "earnings_group"] <- 2
data[data$earnings >  second_limit, "earnings_group"] <- 3

lm(earnings_group ~ average_strokes, data)
summary(lm(earnings_group ~ average_strokes, data))

## Whether or not it's a better analysis is a qualitative question
## open for interpretation. My take is that: if golf players are
## actually classified into groups (three in this case), and you
## want to understand how does the average of strokes per round
## affect it's movement across this three groups, then it's is
## a good way to do it, and, as can be seen, for each stroke
## a player improves in avareage per round, it's brining him/her
## 0.7 units closer to moving up in the earnings groups. This means
## that you need to improve 1.2965 (1 / 0.7713) average strokes
## per round to move up a group.
##
## In the other hand, if you want to understand the effect of
## avarege strokes per round in earnings in general (without
## considering groups), then the first kind of analysis is better.
## As we saw, for each stroke that is reduced on average per round,
## the player can expect an increase of $79,915 in earnings.
##
## So the answer is: it depends on what you're trying to understand
##

##
## b)
##

lm(average_strokes ~ ., data[, seq(3, 26)])
summary(lm(average_strokes ~ ., data[, seq(3, 26)]))

## When we include all variables (except the name and the rank which
## are directly correlated with stroke_average per round) we find that
## the most statistically significant, hence better predictors, for
## stroke avarege are: events, rounds_played, total_adjustment, ott,
## app, putt, and gir. Note that I only used those which have a
## significance level less than 0.001.

lm(
    average_strokes ~
        ott + app + putt + total_putts + putts_per_round + fairway + gir,
    data
)
summary(lm(
    average_strokes ~
        ott + app + putt + total_putts + putts_per_round + fairway + gir,
    data
))

## When we include only skill factors (strokes gained, putts, precision),
## we find that strokes gained and putts are very statistically significant.
## However, from precision, gir also is statistically significant but at
## a little bit lower level than the other variables.

##
## c)
##

## The way I'm interpreting this question is as follows: get the predicted
## earnings for each player according to the results of the regression
## that uses the skill factors, and get difference among this predicted
## earnigs and their actual recorded earnings, and rank by this difference.
## Note that since the question is asking for those who win "more" than
## their skill-numbers indicate, the direction of the difference is important
## and we want to use: 'realized - predicted' instead of 'predicted - realized'.

## Note that the predictive power in this case is lower than for the other
## cases, meaning that we may see big changes in the ranking order.
model <- lm(
    earnings ~
        ott + app + putt + total_putts + putts_per_round + fairway + gir,
    data
)
summary(model)

predicted_earnings <- predict(model)
data$earnings_difference <- (data$earnings - predicted_earnings)
data[with(data, order(-earnings_difference)), "rank_by_earnings_difference"] <- seq(1, nrow(data))

## The player ordering by earnings difference among realized and predicted is:
data[with(data, order(rank_by_earnings_difference)), "player"]
